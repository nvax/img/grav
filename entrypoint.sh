#!/usr/bin/env bash

echo "Set Logo-Text of default Logo to '$LOGO_TEXT', in following files:"
find . -name 'nvax-logo.svg'
find . -name 'nvax-logo.svg' -exec sed -i 's/{{$LOGO_TEXT}}/'"$LOGO_TEXT"'/g' {} \;

#for dir in /var/www/html/user/themes/*; do (cat /nvax/logo ); done

exec "$@"
