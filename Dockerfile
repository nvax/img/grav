FROM registry.gitlab.com/nvax/img/grav/base
ENV LOGO_TEXT "\$LOGO_TEXT"

USER www-data
WORKDIR /var/www/html

# -- install themes
RUN ./bin/gpm install quark
RUN ./bin/gpm install darkquark

# -- remove default pages
RUN rm -rf user/pages
RUN mkdir -p user/pages/01.home
COPY templates/default.md user/pages/01.home/default.md

# -- grav config
COPY templates/config user/config

# -- customize themes
# quark
COPY templates/logo.svg user/themes/quark/images/logo/nvax-logo.svg
COPY templates/quark.config.yaml user/themes/quark/quark.yaml
COPY templates/quark.custom.css user/themes/quark/css/custom.css
# darkquark
COPY templates/logo-dark.svg user/themes/darkquark/images/logo/nvax-logo.svg
COPY templates/quark.config.yaml user/themes/darkquark/darkquark.yaml
COPY templates/quark.custom.css user/themes/darkquark/css/custom.css

WORKDIR /var/www/html
USER root

COPY --chown=www-data:www-data --chmod=777 entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["sh", "-c", "cron && apache2-foreground"]
